import Vue from 'vue'
import App from './App'
import './uni.promisify.adaptor'

Vue.config.productionTip = false

//引入UI框架配置，uview
import uView from "uview-ui";
Vue.use(uView);
//UI引用配置结束

App.mpType = 'app'

const app = new Vue({
  ...App
})
app.$mount()
